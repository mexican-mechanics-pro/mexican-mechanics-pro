#include<iostream>
#include<locale.h>
using namespace std;


int main(){
	int res;
setlocale(LC_ALL,"");
	do{
		
		//Opci�n 1 es para el primer piso y opcion 2 para el segundo
		int opcion, opcion2;
		//Primer piso
		bool ventanas, luces, porton, escaleras;
		
		//Segundo piso
		bool ventanas2, luces2, puertas2;
		
		//Variables auxiliares, aux para piso 1 y aux2 para piso 2
		bool aux, aux2;
		
		cout << "\t Hola Usuario \n"<< endl;
		
		cout << "Bienvenido a Casa"<< endl;
		cout << "Seleccionar una de las siguientes opciones:\n"<< endl;
		
		cout << "1.- Entrar a la casa.\n2.- Irse." << endl;
		cout << "\nEscribir la opcion 1 o 2 y presionar enter en tu teclado: ";
		
		
		
		//Leer respuesta
		cin >> res;
		
		
		//Si res es igual a 1 entonces entra al ciclo While hasta la linea 174
		while(res==1){
		
			cout << "\n\tHola Usuario\n" << endl;
			cout << "Seleccionar una de las siguientes opciones:\n"<< endl;	
		
			cout << "1.- Ventanas.\n2.- Luces.\n3.- Port�n.\n4.- Escaleras.\n5.- Mostrar situacion.\n6.- Salir.\n" << endl;
			cout << "Escribir una opcion del 1 al 6 y presionar enter en tu teclado: ";
			cin >>	opcion;
			
			
			/*
				Switch o interruptor. Dependiendo del valor que se le asigno a opcion se eligirá un caso.
				El caso 1 es para abrir o cerrar ventanas
				El 2 Para las luces
				3 para porton
				4 Para subir o bajar escaleras
				5 Para mostrar la situacion actual de la casa
				6 Para salir del programa
			*/
			switch(opcion){
				case 1:	cout << "Abrir ventanas (1)	Cerrar ventanas (0):";
						cin >> aux;
						
						//Si auxiliar es igual a ventanas, entonces se realiza la instruccion, sino se hace lo del else
						if(aux == ventanas){//Si se quiere hacer una accion que ya se realizo, por logica no lo poermitira
							cout << "\tEsta acci�n no se puede repetir.\n\n";
						}else{
							ventanas = aux;
							
							if(ventanas == true)
								cout << "\n\tVentanas abiertas\n\n";
							else
								cout << "\n\tVentanas cerradas\n\n";
						}
						break;
				case 2:	cout << "Encender luces (1)	Apagar luces (0): ";
						cin >> aux;
						if(aux == luces){
							cout << "Esta accion no se puede repetir.\n\n";
						}else{
							luces = aux;
							if(luces)
								cout << "\n\tLuces encendidas.\n\n";
							else
								cout << "\n\tLuces apagadas.\n\n"; 
						}
						break;
				case 3:	cout << "Abrir port�n (1)	Cerrar port�n	(0): ";
						cin >> aux;
						
						if(aux == porton){
							cout << "Esta accion no se puede repetir.\n\n";
						}else{
							porton = aux;
							
							if(porton == true)
								cout << "\n\tPort�n abierto.\n\n";
							else
								cout << "\n\tPort�n cerrado.\n\n";
						}
						break;
						
						
				case 4:	cout <<"Subir escaleras	(1)	Bajar escaleras	(0): ";
						cin >>	aux;
						if(aux	==	escaleras){
							cout << "Esta acci�n no se puede repetir.\n\n";
						}else{//Se le asigna el nuevo valor a la variable escalera. Si es true, entonces esta arriba, si es false entonces esta abajo
							escaleras = aux;
							if(escaleras == true){
								cout << "\n\tSubiendo escaleras...\n\tSe encuentra arriba\n\n";
							
							
							/*
								En este doWhile se hace lo mismo como si estuvieras en el primer piso, la diferencia
								es hay otras variables con el mismo nombre, solo que se le agrega un 2, indicando que
								es el segundo piso
							
							*/
							do{
								cout << "\n\tHola Usuario\n" << endl;
								cout << "1.- Ventanas\n2.- Luces\n3.- Puertas\n4.- Bajar escaleras\n" << endl;
								cout << "Escribir una opcion del 1 al 4 y presiona enter: ";
								cin >> opcion2;
								
								/*
									Un switch muy similar al del primer piso, la diferencia es que aqui se
									trabajan con otras variables, las del segundo piso.
								*/
								
								switch(opcion2){
									case 1:cout << "Abrir ventanas (1)	Cerrar ventanas (0): ";
										cin >> aux2;
										
										if(aux2 == ventanas2){
											cout << "\tEsta acci�n no se puede repetir.\n\n";
										}else{
											ventanas2 = aux2;
										if(ventanas2 == true)
											cout << "\n\tVentanas abiertas\n\n";
										else
											cout << "\n\tVentanas cerradas\n\n";
									}
											break;
									case 2:cout << "Encender luces (1)	Apagar luces (0): ";
										cin >> aux2;
										if(aux2 == luces2){
										cout << "Esta acci�n no se puede repetir.\n\n";
										}else{
											luces2 = aux2;
										if(luces2)
											cout << "\n\tLuces encendidas.\n\n";
										else
											cout << "\n\tLuces apagadas.\n\n"; 
										}
											break;
									case 3:cout << "Abrir puertas (1)	Cerrar puertas (0): ";
										cin >> aux2;
										if(aux2 == puertas2){
											cout << "Esta accion no se puede repetir.\n\n";
										}else{
											puertas2 = aux2;
											if(puertas2)
												cout << "\n\tPuertas abiertas.\n\n";
											else
												cout << "\n\tPuertas cerradas.\n\n"; 
										}
											break;
									/*
										Si el usuario quiere bajar las escaleras, entonces se le asigna el nuevo valor de false.
										Primer piso =  false
										Segundo piso = true
									*/
									case 4:	escaleras = false;
											if(escaleras == false)
												cout << "\n\tBajando escaleras...\n\tSe encuentra abajo\n\n";
											break;
											
									default:	break;
								}
								
								/*
									Mientras se cumpla la condicion de que escaleras es igual a true, se permanecera en el ciclo
									hasta que el usuario decida cambiarlo a false en el caso 4.
								*/
							}while(escaleras == true);
								
						}
							
						}
						break;
						
						
				case 5:	
				cout << "\n\tHola Usuario, esta es la situacion de tu casa\n" << endl;
				cout << "\n\tPrimer piso\n" << endl;
				
				/*
					Aqui se imprime todo el estatus de las variables del primer piso
					1 o true es que esta abierto o encendido.
					0 o false es que esta cerrado o apagado.
				*/
						if(ventanas == true)	cout << "1.-Las ventanas se encuentran abieras." << endl;
						else	cout << "1.-Las ventanas se encuentran cerradas." << endl;
						
						if(luces == true)	cout << "2.-Las luces se encuentran encendidas." << endl;
						else	cout << "2.-Las luces se encuentran apagadas."	<< endl;
						
						if(porton == true)	cout << "3.-El porton esta abierto." << endl;
						else	cout << "3.-El porton esta cerrado." << endl;
						
						if(escaleras == true)	cout << "4.-Se encuentra en el segundo piso." << endl;
						else	cout << "4.-Se encuentra en el primer piso." << endl;
						
						cout << "\n\n\tSegundo piso\n" << endl;
						if(ventanas2 == true)	cout << "1.-Las ventanas se encuentran abieras." << endl;
						else	cout << "1.-Las ventanas se encuentran cerradas." << endl;
						
						if(luces2 == true)	cout << "2.-Las luces se encuentran encendidas." << endl;
						else	cout << "2.-Las luces se encuentran apagadas."	<< endl;
						
						if(puertas2 == true)	cout << "3.-Las puertas se encuentran abiertas." << endl;
						else	cout << "3.-Las puertas se encuentran cerradas."	<< endl;
						
						break;
						
						/*
							En este caso, si el usuario elige la accion 6, entonces la variable respuesta
							tendr� otro nuevo  valor, 2
						*/
				case 6:	res = 2;
						break;		
						
						//Si se ingresa una opci�n diferente de las mencionadas, el programa lo ignorar� y repetir� el ciclo
				default:	break;
				
			}
			
		}
	
		//Se va a repetir el ciclo siempre y cuando la variable res sea diferente de 2.
	}while(res!=2);
	
	cout << "\nGracias, buen dia!";
	return 0;
}
